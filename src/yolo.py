import numpy as np


# Horvitz Estimator
def E(R, sample_size):
    """ E[ X_i ] """
    p = 1 / len(R)
    return sample_size * p


def Y(R, sample_size):
    """ Horvitz - Thompson Estimation """
    # S = np.random.choice(R, size=sample_size, replace=True).tolist()
    S = [1, 4, 1, 12, 15]
    print(np.var(S))
    # return sum(S) / E(R, sample_size)

Y([], 2)
# runs = 100000
# x = [7, 7, 8, 9, 9, 9, 10, 14, 14, 15, 17, 17, 20, 21, 25]
# size = int(np.ceil(.33 * len(x)))
# _Y = [Y(x, size) for i in range(runs)]
# Y_mean = np.mean(_Y)
# Y_var = np.std(_Y)
# print('x_sum ( estimate ):', Y_mean, '+-', Y_var)
# print('x_sum ( actual ):', sum(x))
# print(np.var(x), np.var(_Y), (np.var(x) / size) * (len(x) ** 2))
